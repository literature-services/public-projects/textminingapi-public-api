package uk.ac.ebi.literature.textminingapi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@SpringBootTest
@ActiveProfiles(profiles = "testAPI")
@AutoConfigureMockMvc
public class TextminingApiPublicControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
	private TextminingApiPublicService service;
    
    @MockBean
	private MLQueueSenderService rabbitService;
	
	@MockBean
	private MongoService mongoService;
	
	@MockBean
	private TextminingApiPublicValidator validator;
	
    @SpyBean
    private TextminingApiPublicController controller;
	
    private SubmissionMessage submission;

	private AtomicReference<List<String>> errors;
	
	private AnnotationsData annotationData;
	    
    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @BeforeEach
    public void setup() throws IOException {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        
		
		this.submission = Utility.parseJsonObject(TextminingApiPublicTestConstants.submissionData, SubmissionMessage.class);
		this.errors = new AtomicReference<List<String>>(new ArrayList<String>());
		this.annotationData = Utility.parseJsonObject(TextminingApiPublicTestConstants.annotationData, AnnotationsData.class);
		annotationData.setUser(null);
		annotationData.set_id(null);
		annotationData.setDateInserted(null);
		annotationData.setDateModified(null);
    }

    @WithMockUser
    @Test
    public void testHelloMessage() throws Exception {
        this.mockMvc
                .perform(get("/greetings"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .string(containsString("Hello, World")));
    }

    @WithMockUser
    @Test
    public void testSubmit() throws Exception {
        
        when(service.processSubmission(any(), any())).thenReturn(true);
        
        mockMvc.perform(post("/submit")
                .content(Utility.asJsonString(submission))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
 
        when(service.processSubmission(any(), any())).thenReturn(false);
        mockMvc.perform(post("/submit")
                .content(Utility.asJsonString(submission))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
    
    @WithMockUser
    @Test
    public void testDeletion() throws Exception {
        
        when(service.processDeletion(any(), any(), any())).thenReturn(true);
        mockMvc.perform(delete("/delete/"+TextminingApiPublicTestConstants.TEST_FT_ID))
                .andExpect(status().isOk());
 
        checkErrorDeletion(HttpStatus.INTERNAL_SERVER_ERROR);
        checkErrorDeletion(HttpStatus.BAD_REQUEST);
    }
    
    private void checkErrorDeletion(HttpStatus errorStatus) throws Exception {
    	when(service.processDeletion(any(), any(), any())).thenReturn(false);
    	Mockito.doReturn(errorStatus).when(this.controller).getStateFromServiceResponse(any());
    	mockMvc.perform(delete("/delete/"+TextminingApiPublicTestConstants.TEST_FT_ID))
    	.andExpect(status().is(errorStatus.value()));
    }
    
    @WithMockUser
    @Test
    public void testGetAnnotations() throws Exception {
        
        when(service.getAnnotationsData(any(), any())).thenReturn(annotationData);
        
        MvcResult result = mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID+"/"+ TextminingApiPublicTestConstants.TEST_FILENAME))
                .andExpect(status().isOk()).andReturn();
        
        String bodyResponse = result.getResponse().getContentAsString();
        
        assertFalse(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_USER));
        assertTrue(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_FT_ID));
 
        mockMvc.perform(get("/getAnnotations/"))
                .andExpect(status().isNotFound());
        
        when(service.getAnnotationsData(any(), any())).thenReturn(null);
        mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID+"/"+ TextminingApiPublicTestConstants.TEST_FILENAME))
        .andExpect(status().isNotFound());
            
    }
    
    @WithMockUser
    @Test
    public void testGetAnnotationsList() throws Exception {
        
        when(service.getAnnotationsData(any())).thenReturn(List.of(annotationData, annotationData));
        
        MvcResult result = mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
                .andExpect(status().isOk()).andReturn();
        
        String bodyResponse = result.getResponse().getContentAsString();
        
        assertFalse(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_USER));
        assertTrue(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_FT_ID));
 
        mockMvc.perform(get("/getAnnotations/"))
                .andExpect(status().isNotFound());
        
        when(service.getAnnotationsData(any())).thenReturn(null);
        mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
        .andExpect(status().isNotFound());
        
    }
    
    @WithMockUser
    @Test
    public void testGetSubmissionsStatus() throws Exception {
        
        when(service.getSubmissionStatus(any())).thenReturn(this.submission);
        
        MvcResult result = mockMvc.perform(get("/getSubmissionStatus/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
                .andExpect(status().isOk()).andReturn();
        
        String bodyResponse = result.getResponse().getContentAsString();
        
        assertFalse(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_USER));
        assertTrue(bodyResponse.contains(TextminingApiPublicTestConstants.TEST_FT_ID));
 
        mockMvc.perform(get("/getSubmissionStatus/"))
                .andExpect(status().isNotFound());
        
        when(service.getSubmissionStatus(any())).thenReturn(null);
        mockMvc.perform(get("/getSubmissionStatus/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
        .andExpect(status().isNotFound());
        
        
    }
    
    
    @Test
    public void testSecurity() throws Exception {
        this.mockMvc
                .perform(get("/greetings"))
                .andExpect(status().isUnauthorized());
        
        this.mockMvc
        .perform(post("/submit")
        .content(Utility.asJsonString(submission))
        .contentType(APPLICATION_JSON))
        .andExpect(status().isUnauthorized());
        
        this.mockMvc
        .perform(delete("/delete/{ft_id}", TextminingApiPublicTestConstants.TEST_FT_ID)
        .content(Utility.asJsonString(submission))
        .contentType(APPLICATION_JSON))
        .andExpect(status().isUnauthorized());
        
        mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID+"/"+ TextminingApiPublicTestConstants.TEST_FILENAME))
        .andExpect(status().isUnauthorized());
        
        mockMvc.perform(get("/getAnnotations/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
        .andExpect(status().isUnauthorized());
        
        mockMvc.perform(get("/getSubmissionStatus/"+ TextminingApiPublicTestConstants.TEST_FT_ID))
        .andExpect(status().isUnauthorized());
        
        this.mockMvc
        .perform(get("/actuator/health"))
        .andExpect(status().is5xxServerError());
        
        this.mockMvc
        .perform(get("/actuator/prometheus"))
        .andExpect(status().isNotFound());
    }

}