package uk.ac.ebi.literature.textminingapi;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import uk.ac.ebi.literature.textminingapi.pojo.FileInfo;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@SpringBootTest
@ActiveProfiles(profiles = "testAPI")
public class TextminingApiPublicValidatorTest {

	@SpyBean
	private TextminingApiPublicValidatorImpl validator;
	
	@MockBean
	private TextminingApiPublicService service;
	
	@SpyBean
	MongoService mongoService;
	
	@MockBean
	private TextminingApiPublicController controller;
	
	private SubmissionMessage submission;
	
	private SubmissionMessage previousSubmission;

	private AtomicReference<List<String>> errors;
	private AtomicReference<SubmissionMessage> previousData;
	
	@BeforeEach
	public void setup() throws IOException {
		this.submission = Utility.parseJsonObject(TextminingApiPublicTestConstants.submissionData, SubmissionMessage.class);
		this.previousSubmission = Utility.parseJsonObject(TextminingApiPublicTestConstants.previousSubmissionData, SubmissionMessage.class);
		this.previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_FT_ID);
		this.errors = new AtomicReference<List<String>>(new ArrayList<String>());
		this.previousData = new AtomicReference<SubmissionMessage>();
	}
	
	@Test
	public void testInit() {
		assertTrue(validator!=null);
		assertTrue(submission!=null);
		assertTrue(previousSubmission!=null);
	}
	
	@Test
	public void testSubmissionNull() {
		
		assertFalse(validator.validateSubmission(null, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.NULL_ERROR));
		
		this.errors.get().clear();
		assertFalse(validator.validateSubmission(null, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.NULL_ERROR));
	
	}
	
	@Test
	public void testSubmissionFtIdEmptyError() {
		submission.setFtId(null);
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_NOT_EXISTING_IN_EPMC_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.SUBMISSION_ALREADY_EXISTING_ERROR));

	}
	
	@Test
	public void testSubmissionFtIdNotInEpmcErrorButAlreadyPending() {
		submission.setFtId("11");
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_NOT_EXISTING_IN_EPMC_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_ERROR));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.SUBMISSION_ALREADY_EXISTING_ERROR));
		assertTrue(previousSubmission.equals(this.previousData.get()));
	}
	
	@Test
	public void testSubmissionFtIdNotInEpmcErrorNotAlreadyPending() {
		submission.setFtId("11");
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_NOT_EXISTING_IN_EPMC_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.SUBMISSION_ALREADY_EXISTING_ERROR));
      
	}
	
	//@Test
	public void testDuplicateFileInSubmissionError() {
		Stream.of(previousSubmission.getFiles()).forEach(file->file.setFilename("supp_40_2_884__index.html"));
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(previousSubmission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.DUPLICATE_FILENAME_ERROR, "supp_40_2_884__index.html")));
	}
	
	@Test
	public void testSubmissionCallbackEmptyError() {
		submission.setCallback(null);
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.CALLBACK_EMPTY_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.CALLBACK_INVALID_URL_ERROR));
		assertTrue(this.errors.get().size()==1);
		
	}
	
	@Test
	public void testSubmissionCallbackInvalidUrlError() {
		/*submission.setCallback("aaaaa");
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.CALLBACK_INVALID_URL_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.CALLBACK_EMPTY_ERROR));
		assertTrue(this.errors.get().size()==1);*/
		
	}
	
	@Test
	public void testSubmissionNoFileError() {
		submission.setFiles(null);
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.NO_FILE_ERROR));
	    assertTrue(this.errors.get().size()==1);
	    
	    submission.setFiles(new FileInfo[0]);
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.NO_FILE_ERROR));
	    assertTrue(this.errors.get().size()==1);
	}
	
	@Test
	public void testSubmissionFileErrors() {
		submission.getFiles()[0].setFilename(null);
		submission.getFiles()[0].setUrl("aaaa");
		submission.getFiles()[1].setUrl(null);
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.NO_FILE_ERROR));
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.FILE_NAME_EMPTY_ERROR, 1)));
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.FILE_URL_NOT_VALID_ERROR, 1)));
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.FILE_URL_EMPTY_ERROR, 2)));
		assertTrue(this.errors.get().size()==3);
	}
	
	@Test
	public void testSubmissionEpmcApiCallError() throws Exception {
		Mockito.doThrow(new Exception("EPMC API error call")).when(this.validator).makeEPMCcall(any());
		Mockito.doReturn(null).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_NOT_EXISTING_IN_EPMC_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_ERROR));
		assertFalse(this.errors.get().contains(TextminingApiPublicValidator.SUBMISSION_ALREADY_EXISTING_ERROR));
		assertTrue(this.previousData.get() == null);
	}
	
	@Test
	public void testSubmissionValid() {
		Mockito.doReturn(null).when(this.mongoService).findSubmission(any(), any());
		assertTrue(validator.validateSubmission(submission, this.errors, this.previousData));
		assertTrue(this.errors.get().size()==0);
		assertTrue(this.previousData.get()==null);
	}
	
	@Test
	public void testDeletionNullFtId() {
		previousSubmission.setStatus(Status.PENDING.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateDeletion(null, TextminingApiPublicTestConstants.TEST_USER, this.errors));
		assertTrue(this.errors.get().size()==1);
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_URL_PATH_DELETE_ERROR));
		
		this.errors.get().clear();
		assertFalse(validator.validateDeletion("", TextminingApiPublicTestConstants.TEST_USER, this.errors));
		assertTrue(this.errors.get().size()==1);
		assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_URL_PATH_DELETE_ERROR));
	
	}
	
	@Test
	public void testDeletionPendingSubmissionExisting() {
		previousSubmission.setStatus(Status.PENDING.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER, this.errors));
		assertTrue(this.errors.get().size()==1);
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.SUBMISSION_PENDING_ERROR_DELETION, TextminingApiPublicTestConstants.TEST_FT_ID)));
	}
	
	@Test
	public void testDeletionSubmissionNotExisting() {
		Mockito.doReturn(null).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER, this.errors));
		assertTrue(this.errors.get().size()==1);
		assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.SUBMISSION_NOT_FOUND_ERROR, TextminingApiPublicTestConstants.TEST_FT_ID)));
	}
	
	public void testDeletionValid() {
		previousSubmission.setStatus(Status.SUCCESS.getLabel());
		Mockito.doReturn(previousSubmission).when(this.mongoService).findSubmission(any(), any());
		assertFalse(validator.validateDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER, this.errors));
		assertTrue(this.errors.get().size()==0);
	}
	
}
