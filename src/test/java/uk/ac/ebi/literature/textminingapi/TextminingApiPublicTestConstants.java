package uk.ac.ebi.literature.textminingapi;

public class TextminingApiPublicTestConstants {

	protected final static String TEST_FT_ID="PMC8800430";
	
	protected final static String TEST_USER="TEST_USER";
	
	protected final static String TEST_FILENAME="supp_40_2_884__index.html";
	
	protected final static String annotationsData = "[" +
			"{" + 
			"\"ft_id\":\"PMC8800430\"," + 
			"\"filename\":\"supp_40_2_884__index.html\"," + 
			"\"user\":\"TEST_USER\"," + 
			"\"anns\":[" + 
			"{" + 
			"\"exact\": \"RAS\"," + 
			"\"tags\":[" + 
			"{\"name\": \"RAS\","+ 
			"\"uri\": \"http://purl.uniprot.org/uniprot/P01114\"}" + 
			"]," + 
			"\"type\": \"Gene_Proteins\"," + 
			"\"frequency\": 2" + 
			"}," + 
			"{" + 
			"\"exact\": \"BEV\"," + 
			"\"tags\": [" + 
			"{\"name\": \"BEV\","+
			"\"uri\": \"http://identifiers.org/taxonomy/801\" }" + 
			"]," + 
			"\"type\": \"Organisms\"," + 
			"\"frequency\": \"5\"" + 
			"}" + 
			"]" + 
			"},"+
			"{" + 
			"\"ft_id\":\"PMC8800430\"," + 
			"\"filename\":\"supp_gkr715_nar-02838-a-2010-File008.pdf\"," + 
			"\"user\":\"TEST_USER\"," + 
			"\"anns\": [" + 
			"{" + 
			"\"exact\": \"RAS\"," + 
			"\"tags\": [" + 
			"{\"name\": \"RAS\","+
			"\"uri\": \"http://purl.uniprot.org/uniprot/P01114\"}" + 
			"]," + 
			"\"type\": \"Gene_Proteins\"," + 
			"\"frequency\": \"20\"" + 
			"}," + 
			"{" + 
			"\"exact\": \"BEV\"," + 
			"\"tags\": [" + 
			"{\"name\": \"BEV\","+
			 "\"uri\": \"http://identifiers.org/taxonomy/801\"}" + 
			"]," + 
			"\"type\": \"Organisms\"," + 
			"\"frequency\": \"10\"" + 
			"}" + 
			"]" + 
			"}," +
			"{" + 
				"\"ft_id\":\"PMC8800430222\"," + 
				"\"filename\":\"supp_gkr715_nar-02838-a-2010-File008.pdf\"," + 
				"\"user\":\"TEST_USER\"," + 
				"\"anns\": [" + 
				"{" + 
				"\"exact\": \"RAS\"," + 
				"\"tags\": [" + 
				"{\"name\": \"RAS\","+
				"\"uri\": \"http://purl.uniprot.org/uniprot/P01114\"}" + 
				"]," + 
				"\"type\": \"Gene_Proteins\"," + 
				"\"frequency\": \"20\"" + 
				"}," + 
				"{" + 
				"\"exact\": \"BEV\"," + 
				"\"tags\": [" + 
				"{\"name\": \"BEV\","+
				"\"uri\": \"http://identifiers.org/taxonomy/801\"}" + 
				"]," + 
				"\"type\": \"Organisms\"," + 
				"\"frequency\": \"10\"" + 
				"}" + 
				"]" + 
				"}" +
			"]";
	
	protected static final String annotationData=
			"{" + 
			"\"ft_id\":\"PMC8800430\"," + 
			"\"filename\":\"supp_40_2_884__index.html\"," + 
			"\"user\":\"TEST_USER\"," + 
			"\"anns\":[" + 
			"{" + 
			"\"exact\": \"RAS\"," + 
			"\"tags\":[" + 
			"{\"name\": \"RAS\","+ 
			"\"uri\": \"http://purl.uniprot.org/uniprot/P01114\"}" + 
			"]," + 
			"\"type\": \"Gene_Proteins\"," + 
			"\"frequency\": 2" + 
			"}," + 
			"{" + 
			"\"exact\": \"BEV\"," + 
			"\"tags\": [" + 
			"{\"name\": \"BEV\","+
			"\"uri\": \"http://identifiers.org/taxonomy/801\" }" + 
			"]," + 
			"\"type\": \"Organisms\"," + 
			"\"frequency\": \"5\"" + 
			"}" + 
			"]" + 
			"}";
	
	protected final static String submissionData="{\r\n" + 
			"\r\n" + 
			"    \"ft_id\": \"PMC8800430\",\r\n" + 
			"\r\n" + 
			"    \"files\":[      \r\n" + 
			"\r\n" + 
			"           {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_40_2_884__index.html\",\r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_40_2_884__index.html\"         },\r\n" + 
			"\r\n" + 
			"    {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File008.pdf\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File008.pdf\"\r\n" + 
			"\r\n" + 
			"        },\r\n" + 
			"\r\n" + 
			"       {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File009.pdf\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File009.pdf\"\r\n" + 
			"\r\n" + 
			"       },\r\n" + 
			"\r\n" + 
			"       {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File007.doc\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File007.doc\"\r\n" + 
			"\r\n" + 
			"       }        ],\r\n" + 
			"\r\n" + 
			"    \"callback\" : \"https://www.ebi.ac.uk/biostudies/callbackSupplFiles\" \r\n" + 
			"\r\n" + 
			"}";
	
	protected final static String previousSubmissionData="{\r\n" + 
			"\r\n" + 
			"    \"ft_id\": \"PMC8800430\",\r\n" + 
			"    \"status\": \"pending\",\r\n" + 
			"\r\n" + 
			"    \"files\":[      \r\n" + 
			"\r\n" + 
			"           {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_40_2_884__index.html\",\r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_40_2_884__index.html\"         },\r\n" + 
			"\r\n" + 
			"    {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File008.pdf\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File008.pdf\"\r\n" + 
			"\r\n" + 
			"        },\r\n" + 
			"\r\n" + 
			"       {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File009.pdf\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File009.pdf\"\r\n" + 
			"\r\n" + 
			"       },\r\n" + 
			"\r\n" + 
			"       {             \r\n" + 
			"\r\n" + 
			"           \"filename\" : \"supp_gkr715_nar-02838-a-2010-File007.doc\",             \r\n" + 
			"\r\n" + 
			"           \"url\" : \"https://www.ebi.ac.uk/biostudies/files/S-EPMC8800430/supp_gkr715_nar-02838-a-2010-File007.doc\"\r\n" + 
			"\r\n" + 
			"       }        ],\r\n" + 
			"\r\n" + 
			"    \"callback\" : \"https://www.ebi.ac.uk/biostudies/callbackSupplFiles\" \r\n" + 
			"\r\n" + 
			"}";
}
