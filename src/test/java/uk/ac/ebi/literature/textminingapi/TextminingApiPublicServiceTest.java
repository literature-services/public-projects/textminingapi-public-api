package uk.ac.ebi.literature.textminingapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.FileInfo;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;


@SpringBootTest
@ActiveProfiles(profiles = "testAPI")
public class TextminingApiPublicServiceTest {

	@SpyBean
	private TextminingApiPublicService textminingApiService;

	@MockBean
	private MLQueueSenderService rabbitService;
	
	@MockBean
	private MongoService mongoService;
	
	@MockBean
	private TextminingApiPublicValidator validator;
	    
	private SubmissionMessage submission;
	
	private SubmissionMessage previousSubmission;

	private AtomicReference<List<String>> errors;
	
	private AtomicReference<SubmissionMessage> previousData;
	
	private AtomicReference<HttpStatus> status;
	
	private AnnotationsData annotationData;

	@BeforeEach
	public void setup() throws IOException {
		
		this.submission = Utility.parseJsonObject(TextminingApiPublicTestConstants.submissionData, SubmissionMessage.class);
		this.previousSubmission = Utility.parseJsonObject(TextminingApiPublicTestConstants.previousSubmissionData, SubmissionMessage.class);
		this.annotationData = Utility.parseJsonObject(TextminingApiPublicTestConstants.annotationData, AnnotationsData.class);
		this.errors = new AtomicReference<List<String>>(new ArrayList<String>());
		this.status = new AtomicReference<HttpStatus>();
		this.previousData = new AtomicReference<SubmissionMessage>();
		Mockito.doReturn(TextminingApiPublicTestConstants.TEST_USER).when(this.textminingApiService).getUsername();
		Mockito.doReturn(true).when(this.textminingApiService).insertSubmissionIntoMongo(any(), any());
		Mockito.doReturn(true).when(this.textminingApiService).deleteSubmissionFromMongo(any(), any());
	}

	
    @Test
    public void testPopulate() throws IOException, TimeoutException {
    	assertTrue(this.submission.getUser()== null && this.submission.getStatus()==null);
    	
    	for (FileInfo fileInfo : submission.getFiles()) {
    		assertTrue(fileInfo.getStatus()==null);
    	}
    	this.textminingApiService.populateData(this.submission);
    	assertTrue(this.submission.getUser().equalsIgnoreCase(TextminingApiPublicTestConstants.TEST_USER) && this.submission.getStatus().equals(Status.PENDING.getLabel()));
    	for (FileInfo fileInfo : submission.getFiles()) {
    		assertTrue(fileInfo.getStatus().equals(Status.PENDING.getLabel()));
    	}
    }
    
    @Test
    public void testInvalidSubmission() throws IOException, TimeoutException {
    	Mockito.doReturn(false).when(this.validator).validateSubmission(any(), any(), any());
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(0)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(0)).sendMessageToQueue(any(), any(), any());
    	  
    }
    
    @Test
    public void testValidSubmission() throws IOException, TimeoutException {
    	Mockito.doReturn(true).when(this.validator).validateSubmission(any(), any(), any());
    	Mockito.doReturn(true).when(this.rabbitService).sendMessageToQueue(any(), any(), any());
    	assertTrue(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(this.submission.getFiles().length)).sendMessageToQueue(any(), any(), any());
    	  
    }
    
    @Test
    public void testValidSubmissionwithRabbitProblems() throws IOException, TimeoutException {
    	Mockito.doReturn(true).when(this.validator).validateSubmission(any(), any(), any());
    	Mockito.doReturn(false).when(this.rabbitService).sendMessageToQueue(any(), any(), any());
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(1)).sendMessageToQueue(any(), any(), any());
    	assertTrue(this.errors.get().size()==1 && this.errors.get().contains(TextminingApiPublicService.NETWORK_ERROR_MSG)); 
    }
    
    @Test
    public void testValidSubmissionwithMongoProblems() throws Exception {
    	Mockito.doReturn(true).when(this.validator).validateSubmission(any(), any(), any());
    	Mockito.doReturn(false).when(this.textminingApiService).insertSubmissionIntoMongo(any(), any());
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(0)).sendMessageToQueue(any(), any(), any());
    	assertTrue(this.errors.get().size()==1 && this.errors.get().contains(TextminingApiPublicService.DB_ERROR_MSG)); 
    }
    
    @Test
    public void testInvalidDeletion() throws IOException, TimeoutException {
    	Mockito.doReturn(false).when(this.validator).validateDeletion(any(), any(), any());
    	assertFalse(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(0)).deleteSubmissionFromMongo(any(), any());
    	  
    }
    
    @Test
    public void testValidDeletion() throws IOException, TimeoutException {
    	Mockito.doReturn(true).when(this.validator).validateDeletion(any(), any(), any());
    	assertTrue(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(1)).deleteSubmissionFromMongo(any(), any());
    	  
    }
    
    @Test
    public void testValidDeletionwithMongoProblems() throws Exception {
    	Mockito.doReturn(true).when(this.validator).validateDeletion(any(), any(), any());
    	Mockito.doReturn(false).when(this.textminingApiService).deleteSubmissionFromMongo(any(), any());
    	assertFalse(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(1)).deleteSubmissionFromMongo(any(), any());
    	
    }
    
    @Test 
    public void testValidGetAnnotations() {
    	Mockito.doReturn(annotationData).when(this.mongoService).findAnnotations(any(), any(), any());
    	assertTrue(textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_FILENAME).equals(annotationData));
    }

    @Test 
    public void testValidGetAnnotationsList() {
    	Mockito.doReturn(List.of(annotationData)).when(this.mongoService).findAnnotations(any(), any());
    	assertTrue(textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID).equals(List.of(annotationData)));
    }
    
    @Test 
    public void testNotFoundGetAnnotations() {
    	Mockito.doReturn(null).when(this.mongoService).findAnnotations(any(), any(), any());
    	assertTrue(textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_FILENAME)==null);
    }
    
    @Test 
    public void testNotFoundGetAnnotationsList() {
    	Mockito.doReturn(List.of()).when(this.mongoService).findAnnotations(any(), any());
    	assertEquals(true, textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID).isEmpty());
    }
    
    @Test 
    public void testValidGetSubmissionStatus() {
    	submission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	Mockito.doReturn(this.submission).when(this.mongoService).findSubmission(any(), any());
    	assertTrue(textminingApiService.getSubmissionStatus(TextminingApiPublicTestConstants.TEST_FT_ID).equals(submission));
    }
    
    @Test 
    public void testNotFoundGetSubmissionStatus() {
    	Mockito.doReturn(null).when(this.mongoService).findSubmission(any(), any());
    	assertTrue(textminingApiService.getSubmissionStatus(TextminingApiPublicTestConstants.TEST_FT_ID)==null);
    }
    
}

