package uk.ac.ebi.literature.textminingapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@SpringBootTest
@ActiveProfiles(profiles = "testAPI")
public class TextminingApiPublicIntegrationTest {

	@SpyBean
	private TextminingApiPublicService textminingApiService;

	@MockBean
	private MLQueueSenderService rabbitService;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@SpyBean
	private MongoService mongoService;
	
	@SpyBean
	private TextminingApiPublicValidator validator;

    private SubmissionMessage submission;
	
	private SubmissionMessage previousSubmission;

	private AtomicReference<List<String>> errors;
	
	private AtomicReference<HttpStatus> status;
	
	private List<AnnotationsData> annotationsData;

	@BeforeEach
	public void setup() throws IOException {
		this.submission = Utility.parseJsonObject(TextminingApiPublicTestConstants.submissionData, SubmissionMessage.class);
		this.previousSubmission = Utility.parseJsonObject(TextminingApiPublicTestConstants.previousSubmissionData, SubmissionMessage.class);
		this.annotationsData = Utility.parseJsonList(TextminingApiPublicTestConstants.annotationsData, AnnotationsData.class);
		this.errors = new AtomicReference<List<String>>(new ArrayList<String>());
		this.status = new AtomicReference<HttpStatus>();
		Mockito.doReturn(TextminingApiPublicTestConstants.TEST_USER).when(this.textminingApiService).getUsername();
		Mockito.doReturn(true).when(this.rabbitService).sendMessageToQueue(any(), any(), any());
		Query query = new Query();
		mongoTemplate.findAllAndRemove(query, SubmissionMessage.class);
		mongoTemplate.findAllAndRemove(query, AnnotationsData.class);
	}
    
	@Test
    public void testInvalidSubmission() throws IOException {
		checkSubmissionCount(0);
		this.submission.setFiles(null);
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(0)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(0)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(0);  
    }
    
    @Test
    public void testValidSubmission() throws IOException {
    	checkSubmissionCount(0);
    	assertTrue(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	SubmissionMessage messageInserted = this.mongoService.findSubmission(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER); 
    	assertTrue(messageInserted.getUser().equals(TextminingApiPublicTestConstants.TEST_USER));
    	assertTrue(messageInserted.getStatus().equals(Status.PENDING.getLabel()));
    	assertTrue(messageInserted.getDateInserted()!=null);
    	assertTrue(messageInserted.getDateModified()!=null);
    	verify(this.rabbitService, Mockito.times(this.submission.getFiles().length)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(1);  
    }
    
    @Test
    public void testMongoErrorSubmission() throws Exception {
    	checkSubmissionCount(0);
    	Mockito.doThrow(new Exception("Mongo Error")).when(this.mongoService).storeSubmissionNoTransactional(any());
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(0)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(0);  
    }
    
    @Test
    public void testPreEexistingPendingSubmissionSameUser() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	mongoService.storeSubmissionNoTransactional(previousSubmission);
    	storeAnnotationsCount(this.annotationsData);
    	checkAnnotationsCount(3);
    	assertFalse(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(0)).insertSubmissionIntoMongo(any(), any());
    	verify(this.rabbitService, Mockito.times(0)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(1); 
    	checkAnnotationsCount(3);
    }
    
    @Test
    public void testPreEexistingPendingSubmissionOtherUser() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser("Other user");
    	mongoService.storeSubmissionNoTransactional(previousSubmission);
    	storeAnnotationsCount(this.annotationsData);
    	checkAnnotationsCount(3);
    	assertTrue(this.textminingApiService.processSubmission(this.submission, this.errors));
    	verify(textminingApiService, Mockito.times(1)).insertSubmissionIntoMongo(any(), any());
    	SubmissionMessage messageInserted = this.mongoService.findSubmission(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER); 
    	assertTrue(messageInserted.getUser().equals(TextminingApiPublicTestConstants.TEST_USER));
    	assertTrue(messageInserted.getStatus().equals(Status.PENDING.getLabel()));
    	assertTrue(messageInserted.getDateInserted()!=null);
    	assertTrue(messageInserted.getDateModified()!=null);
    	verify(this.rabbitService, Mockito.times(this.submission.getFiles().length)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(2); 
    	checkAnnotationsCount(1);
    }
    
    @Test
    public void testPreEexistingFinalizedSubmission() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.FAILED.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	storeAnnotationsCount(this.annotationsData);
    	checkAnnotationsCount(3);
    	ObjectId previousId = previousSubmission.get_id();
    	Date created = previousSubmission.getDateInserted();
    	assertTrue(this.textminingApiService.processSubmission(this.submission, this.errors));
    	SubmissionMessage messageInserted = this.mongoService.findSubmission(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_USER); 
    	assertTrue(messageInserted.getUser().equals(TextminingApiPublicTestConstants.TEST_USER));
    	assertTrue(messageInserted.getStatus().equals(Status.PENDING.getLabel()));
    	assertTrue(created.getTime()==messageInserted.getDateInserted().getTime());
    	assertTrue(created.getTime() < messageInserted.getDateModified().getTime());
    	assertTrue(previousId.equals(messageInserted.get_id()));
    	verify(this.rabbitService, Mockito.times(this.submission.getFiles().length)).sendMessageToQueue(any(), any(), any());
    	checkSubmissionCount(1);  
    	checkAnnotationsCount(1);
    }
    
    private void checkSubmissionCount(int count) {
    	assertEquals(mongoTemplate.estimatedCount(SubmissionMessage.class), count);  
    }
    
    private void checkAnnotationsCount(int count) {
    	assertEquals(mongoTemplate.estimatedCount(AnnotationsData.class), count);  
    }
    
    private void storeAnnotationsCount(List<AnnotationsData> annotationsData) {
    	annotationsData.stream().forEach( annData -> {
			try {
				this.mongoService.storeAnnotationsNoTransactional(annData);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}); ;
    }
    
    @Test
    public void testInvalidDeletionPendingStatusSubmission() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.PENDING.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	checkSubmissionCount(1);
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	assertFalse(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(0)).deleteSubmissionFromMongo(any(), any());
    	assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.SUBMISSION_PENDING_ERROR_DELETION, TextminingApiPublicTestConstants.TEST_FT_ID)));
    	assertTrue(this.errors.get().size()==1);
    	assertTrue(this.status.get() == HttpStatus.BAD_REQUEST);
    	checkSubmissionCount(1); 
    	checkAnnotationsCount(3);
    }
    
    @Test
    public void testInvalidDeletionNotExistingSubmission() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.SUCCESS.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	checkSubmissionCount(1);
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	String notExistingFtId="PMC1111";
    	assertFalse(this.textminingApiService.processDeletion(notExistingFtId, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(0)).deleteSubmissionFromMongo(any(), any());
    	assertTrue(this.errors.get().contains(String.format(TextminingApiPublicValidator.SUBMISSION_NOT_FOUND_ERROR, notExistingFtId)));
    	assertTrue(this.errors.get().size()==1);
    	assertTrue(this.status.get() == HttpStatus.BAD_REQUEST);
    	checkSubmissionCount(1); 
    	checkAnnotationsCount(3);
    }
    
    @Test
    public void testInvalidDeletionNullFtId() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.SUCCESS.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	checkSubmissionCount(1);
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	assertFalse(this.textminingApiService.processDeletion("", this.errors, this.status));
    	verify(textminingApiService, Mockito.times(0)).deleteSubmissionFromMongo(any(), any());
    	assertTrue(this.errors.get().contains(TextminingApiPublicValidator.FT_ID_EMPTY_URL_PATH_DELETE_ERROR));
    	assertTrue(this.errors.get().size()==1);
    	assertTrue(this.status.get() == HttpStatus.BAD_REQUEST);
    	checkSubmissionCount(1); 
    	checkAnnotationsCount(3);
    }
    
    @Test
    public void testInvalidDeletionMongoError() throws Exception {
    	Mockito.doThrow(new Exception("Mongo error in deletion submission")).when(this.mongoService).deleteSubmissionNoTransactional(any(), any());
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.SUCCESS.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	checkSubmissionCount(1);
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	assertFalse(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(1)).deleteSubmissionFromMongo(any(), any());
    	assertTrue(this.errors.get().contains(TextminingApiPublicService.INTERNAL_DELETION_ERRER));
    	assertTrue(this.errors.get().size()==1);
    	assertTrue(this.status.get() == HttpStatus.INTERNAL_SERVER_ERROR);
    	checkSubmissionCount(1); 
    	checkAnnotationsCount(3);
    }
    
    @Test
    public void testValidDeletion() throws Exception {
    	checkSubmissionCount(0);
    	previousSubmission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	previousSubmission.setStatus(Status.SUCCESS.getLabel());
    	previousSubmission = mongoService.storeSubmissionNoTransactional(previousSubmission);
    	checkSubmissionCount(1);
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	assertTrue(this.textminingApiService.processDeletion(TextminingApiPublicTestConstants.TEST_FT_ID, this.errors, this.status));
    	verify(textminingApiService, Mockito.times(1)).deleteSubmissionFromMongo(any(), any());
    	assertTrue(this.errors.get().size()==0);
    	assertTrue(this.status.get() == HttpStatus.OK);
    	checkSubmissionCount(0); 
    	checkAnnotationsCount(1);
    }
    
    @Test
    public void testValidGetAnnotations() throws Exception {
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	AnnotationsData data = this.textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID, TextminingApiPublicTestConstants.TEST_FILENAME);
    	assertTrue(data!=null && (data.getUser()==null) && (data.get_id() == null) &&
    			  (data.getDateInserted() == null) && (data.getDateModified() == null));
    }
    
    @Test
    public void testNotFoundGetAnnotations() throws Exception {
    	this.storeAnnotationsCount(annotationsData);
    	checkAnnotationsCount(3);
    	assertTrue(this.textminingApiService.getAnnotationsData(TextminingApiPublicTestConstants.TEST_FT_ID, "filename_not_existing")==null);
    	assertTrue(this.textminingApiService.getAnnotationsData("ft_id_not_existing", TextminingApiPublicTestConstants.TEST_FILENAME)==null);
    }
    
    @Test
    public void testValidGetSubmissionStatus() throws Exception {
    	submission.setUser(TextminingApiPublicTestConstants.TEST_USER);
    	this.mongoService.storeSubmissionNoTransactional(this.submission);
    	
    	SubmissionMessage data = this.textminingApiService.getSubmissionStatus(TextminingApiPublicTestConstants.TEST_FT_ID);
    	assertTrue(data!=null && (data.getUser()==null) && (data.get_id() == null) &&
    			  (data.getDateInserted() == null) && (data.getDateModified() == null));
    }
    
    @Test
    public void testNotFoundGetSubmissionStatus() throws Exception {
    	assertTrue(this.textminingApiService.getSubmissionStatus(TextminingApiPublicTestConstants.TEST_FT_ID)==null);
    	submission.setUser("OTHER_USER");
    	this.mongoService.storeSubmissionNoTransactional(this.submission);
    	
    	assertTrue(this.textminingApiService.getSubmissionStatus(TextminingApiPublicTestConstants.TEST_FT_ID)==null);
    }
    
}

