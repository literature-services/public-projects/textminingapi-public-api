package uk.ac.ebi.literature.textminingapi;


import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TextminingApiPublicGrafanaConfig {

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
    	return registry -> registry.config().commonTags("application", System.getProperty("HOSTNAME","textmining-api-public"));
    }
}